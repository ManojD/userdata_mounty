
module.exports = function(app) {
  //logs requests
  var logger = require('../logger');

  var morgan = require('morgan');

  app.use(
	morgan(':remote-addr :url :method HTTP/:http-version :user-agent', {
	  immediate: true,
	  stream: {
	    write: message => {
	      logger.info(message.trim());
	    }
	  }
	})
  );

  //logs responses
  app.use(
	morgan('dev', {
	  skip: function(req, res) {
	    return res.statusCode < 400;
	  },
	  stream: process.stderr
	})
  );

  app.use(
	morgan('dev', {
	  skip: function(req, res) {
	    return res.statusCode >= 400;
	  },
	  stream: process.stdout
	})
  );
};
