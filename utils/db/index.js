const mongoose = require("mongoose");
const logger = require("../logger");
const config = require("../../config");

  let database_uri =
  "mongodb+srv://" +
  process.env.MONGO_USERNAME +
  ":" +
  encodeURIComponent(process.env.MONGO_PASSWORD) +
  "@" +
  process.env.MONGO_URI +
  "/" +
  process.env.MONGO_DB_NAME +
  "?retryWrites=true&w=majority"

  const options = {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  autoIndex: true,
  useCreateIndex: true,
  poolSize: 50,
  bufferMaxEntries: 0,
  keepAlive: 120,
  useFindAndModify: false,
};

// console.log("db", database_uri)

mongoose.Promise = global.Promise;
const connection = mongoose.connect(database_uri, options);

connection
  .then((db) => {
    logger.info(
      `Successfully connected to ${config.db.MONGO_URI} MongoDB cluster in ${
        config.server.ENV
      } mode.`
    );
    return db;
  })
  .catch((err) => {
    //console.log({err})
    if (err.message.code === "ETIMEDOUT") {
      logger.info("Attempting to re-establish database connection.");
      mongoose.connect(database_uri);
    } else {
      logger.error("Error while attempting to connect to database:", {
        err,
      });
    }
  });

exports.Connect = () => {
  return connection;
};
