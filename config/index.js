const dotenv = require("dotenv");
const path = require("path");

const configs = {
  dev: {
    config: "dev",
    env: path.resolve(__dirname, "..", ".env.dev"),
  },
  production: {
    config: "prod",
    env: path.resolve(__dirname, "..", ".env.prod"),
  },
  test: {
    config: "test",
    env: path.resolve(__dirname, "..", ".env.test"),
  },
};

// console.log(process.env.NODE_ENV)
const currentEnvironment = process.env.NODE_ENV || "dev";
// console.log(`currentEnvironment .env from '${currentEnvironment}'`);

const defaultPath = path.resolve(__dirname, "..", ".env");
// console.log(`defaultPath .env from '${defaultPath}'`);

const envPath = configs[currentEnvironment].env || defaultPath;

// console.log(`envPath .env from '${configs[currentEnvironment].env}'`);
dotenv.config({ path: envPath });

// console.log(process.env)

const _default = {
  server: {
    PORT: process.env.PORT,
    ENV: process.env.ENV,
    APP: process.env.APP
  },
  jwt: {
    SECRET: process.env.JWT_SECRET,
  },
  db: {
    MONGO_URI: process.env.MONGO_URI,
    MONGO_PORT: process.env.MONGO_PORT,
    MONGO_DB_NAME: process.env.MONGO_DB_NAME,
    MONGO_USERNAME: process.env.MONGO_USERNAME,
    MONGO_PASSWORD: process.env.MONGO_PASSWORD,
  },
  schema: {
    SCHEMA_VERSION: process.env.SCHEMA_VERSION,
  },
};

const config = require(`./${configs[currentEnvironment].config}`);
// let testdata = Object.assign({ env: currentEnvironment }, _default, config);
// console.log(testdata)

module.exports = Object.assign({ env: currentEnvironment }, _default, config);
