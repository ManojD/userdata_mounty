const { body, param, query, validationResult } = require('express-validator');
const ApiResponse = require('../../utils/ApiResponse');

const validateRules = (method) => {
  switch (method) {

    case 'signup':
    {
      return [
        body('email').normalizeEmail().isEmail().withMessage("email is missing"),
        body('password', 'password is missing').notEmpty(),
        body('name', 'name is missing').notEmpty(),
        body('mobile', 'mobile no. is missing').notEmpty(),
      ];
    }

    case 'signin':
    {
      return [
        body('email').normalizeEmail().isEmail().withMessage("email is missing"),
        body('password', 'password is missing').notEmpty()
      ];
    }

    case 'signout':
    {
      return [
        body('sessionId', 'sessionId is missing').notEmpty(),
        body('email').normalizeEmail().isEmail().withMessage("email is missing")
      ];
    }

    case 'create':
    {
      return [
        body('email').normalizeEmail().isEmail().withMessage("email is missing"),
        body('password', 'password is missing').notEmpty(),
      ];
    }

    case 'delete':
    {
      return [
        param('id', 'id required').notEmpty()
      ];
    }

    case 'getByCoordinates':
    {
      return [
        query('lat', 'latitude is missing').notEmpty(),
        query('long', 'longitude is missing').notEmpty()
      ];
    }

  }
};

const validate = (req, res, next) => {
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = errors.array().map( err => {
    return { [err.param]: err.msg } 
  });

  return res.status(422).send(
    new ApiResponse.responseObject(422, null, extractedErrors).getResObject()
  );
};

module.exports = {
  validateRules,
  validate
};
