const _ = require("lodash");
const jwt = require('jsonwebtoken');

const User = require("../models/user.js");
const apiResponse = require("../../utils/ApiResponse.js");

module.exports = {

  //////Sign-Up
  signup: async (req, res) => {
    let userData = req.body;
    try {

      let isUserExistsWithEmail = await User.findOne({ email: userData.email });
      if (isUserExistsWithEmail) {
        return res.status(400).send(
          new apiResponse.responseObject( 400, "User already exists with email", null ).getResObject()
        );
      }
      let tokenData = {
        name: userData.name,
        email: userData.email,
        mobile: userData.mobile,
        address: userData.address,
      }
      let jwtToken = jwt.sign(tokenData, process.env.JWT_SECRET, { expiresIn: 86400 }); // expires in 24 hours
      userData.verificationToken = jwtToken;

      let user = await User.create(userData);
      user.password = undefined;
      
      return res.status(200).send(
        new apiResponse.responseObject(
          200,
          { message: "User created successfully", data: user, token: jwtToken },
          null
        ).getResObject()
      );
    } catch (err) {
      // throwing error response to the client
      return res.status(500).send(
        new apiResponse.responseObject(500, null, err.message).getResObject()
      );
    }
  },

  tokenVerification: async (req, res, next) => {
    try {
      let token = req.params.id;
      if (!token){
        return res.status(401).send({ auth: false, message: 'No token provided.' });
      }

      let decoded = await jwt.verify(token, process.env.JWT_SECRET)

      if(decoded){
        const user = await User.findOneAndUpdate(
          { email: decoded.email },
          { $set: { isEmailVerified: true }
        });
        user.password = undefined
        return res.status(200).send(
          new apiResponse.responseObject(
            200,
            {
              message: "Your email has been verified successfully",
              user: decoded,
              access_token: token
            },
            null
          ).getResObject()
        );
      } else {
        return res.status(400).send(
          new apiResponse.responseObject(400, { message: "Invalid token" }, null ).getResObject()
        );
      }
    } catch (err) {
      // throwing error response to the client
      return res.status(500).send(
        new apiResponse.responseObject(500, null, err.message).getResObject()
      );
    }
  },

// Sign-In
  signin: async (req, res) => {
    try {
      let authenticatedUser = await User.authenticate( req.body.email, req.body.password );
      return res.status(200).send(
        new apiResponse.responseObject(
          200,
          {
            message: "user logged in successfully",
            user: authenticatedUser.data,
            access_token: authenticatedUser.token
          },
          null
        ).getResObject()
      );

    } catch (err) {
      if(err.message === "User_Not_Exist" || err.message === "Wrong_Password"){
        return res.status(400).send(
          new apiResponse.responseObject(400, null, "Invalid username or password").getResObject()
        );
      } else if(err.message === "Email_Not_Verified"){
        return res.status(400).send(
          new apiResponse.responseObject(400, null, "Your email is not verified").getResObject()
        );
      }
      return res.status(500).send(
        new apiResponse.responseObject(500, null, err.message).getResObject()
      );
    }
  },

//JWT token verification
verifyToken: async (req, res, next) => {
    try {
      var token = req.headers['authorization'];
      if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
      let finalToken = token.split("Bearer ");

      jwt.verify(finalToken[1], process.env.JWT_SECRET, function(err, decoded) {
          if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
          
          return next();
      });
    } catch (err) {
      return res.status(500).send(
        new apiResponse.responseObject(500, null, err).getResObject()
      );
    }
  },

  ////Sign-out
  signout: async (req, res) => {
    try {
      // Find user and update sessionId with null
      const isUserLoggedOut = await User.findOneAndUpdate(
        { email: req.body.email, sessionId: req.body.sessionId },
        { $set: { sessionId: null } },
        { new: true }
      );

      if (isUserLoggedOut) {
        return res.status(200).send(
          new apiResponse.responseObject( 200, "User logged out successfully", null).getResObject()
        );
      } else {
        return res.status(404).send(
          new apiResponse.responseObject( 404, "Access denied", null ).getResObject()
        );
      }
    } catch (err) {
      // throwing error response to the client
      return res.status(500).send(
        new apiResponse.responseObject(500, null, err.message).getResObject()
      );
    }
  },

};
