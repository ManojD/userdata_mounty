var User = require("../models/user");
var _ = require('lodash');

const apiResponse = require('../../utils/ApiResponse.js');

module.exports = {

  //creating a user
  create: async (req, res) => {
    try {
      const user = await User.create(req.body);
      user.password = undefined;
      res.status(200).send(
        new apiResponse.responseObject(200, {message: "created Successfully"}, user , null).getResObject()
      );
    } catch(err){
      return res.status(400).send(
        new apiResponse.responseObject(400, null, err).getResObject()
      );
    }
  },

  // updating the user by userID
  update: async(req, res) =>  {
   try{
     var id = req.params.id;
     const updatedProfile = await User.findOneAndUpdate(
       { '_id': id },
       { $set: req.body },
       { new: true }
      )
      updatedProfile.password = undefined

     if (!updatedProfile) {
       // throwing an error if user not found with requested id
       return res.status(404).send(
         new apiResponse.responseObject(404, null, {
           message: 'Profile not found with id: ' + id
         }).getResObject()
       );
     } else {
       // throwing success response
       res.status(200).send(
         new apiResponse.responseObject(
           200,
           updatedProfile,
           null
         ).getResObject()
       );
     }
   } catch (err) {
     // throwing any other errors if occured
     return res
       .status(400)
       .send(new apiResponse.responseObject(400, null, err).getResObject());
   }
  },

  // deleting an user by userID
  delete: async(req, res) =>  {
    try{
      var id = req.params.id;
      const updatedProfile = await User.findByIdAndDelete({ '_id': id })
      updatedProfile.password = undefined
      if (!updatedProfile) {
        // throwing an error if user not found with requested id
        return res.status(404).send(
          new apiResponse.responseObject(404, null, {
            message: 'User deleted successfully with id: ' + id
          }).getResObject()
        );
      } else {
        // throwing success response
        res.status(200).send(
          new apiResponse.responseObject(
            200,
            updatedProfile,
            null
          ).getResObject()
        );
      }
    } catch (err) {
      // throwing any other errors if occured
      return res
        .status(400)
        .send(new apiResponse.responseObject(400, null, err).getResObject());
    }
   },

  //  get all the users 
  getAll: async (req, res) => {
    try{
      let from = req.query.from ? parseInt(req.query.from) : 0;
      let size = req.query.size ? parseInt(req.query.size) : 10;
      let profile = await User.find({})
        .skip(from*size)
        .limit(size)
        profile.password = undefined

      if(profile == null){
        res.status(200).send(
          new apiResponse.responseObject(400, 'No profile data', null).getResObject()
        )
      } else {
        res.status(200).send(
          new apiResponse.responseObject(200, profile , null).getResObject()
        )
      }
    } catch(err) {
      // throwing error response to the user
      return res.status(400).send(
        new apiResponse.responseObject(400, null, err).getResObject()
      );
    }
  },

  // Get all Users sorted by createdAt timestamp with Pagination
  getByCreatedAt: async (req, res) => {
    try {

      let from = req.query.from ? parseInt(req.query.from) : 0;
      let size = req.query.size ? parseInt(req.query.size) : 10;

      let profile = await User.find({})
        .sort({createdAt: 1})
        .skip(from*size)
        .limit(size)
      profile.password = undefined
       
      if (!profile) {
        // throwing an error if user not found
        return res.status(404).send(
          new apiResponse.responseObject(404, null, {
            message: 'Users not found  ' 
          }).getResObject()
        );
      } else {
        // throwing success response if found
        res.status(200).send(
          new apiResponse.responseObject(200, profile, null).getResObject()
        );
      }
    } catch (err) {
      // throwing any other errors if occured
      return res
        .status(400)
        .send(new apiResponse.responseObject(400, null, err).getResObject());
    }
  },

  // Get all Users sorted by their distance from coordinates passed in the query param of the Endpoint. 
  getByCoordinates: async (req, res) => {
    try {

      let from = req.query.from ? parseInt(req.query.from) : 0;
      let size = req.query.size ? parseInt(req.query.size) : 10;

      let profile = await User.aggregate([ {
        $geoNear : {
            near : {
                type : "Point",
                coordinates : [ parseFloat(req.query.lat), parseFloat(req.query.long) ]
            },
            distanceField : "distance",
            distanceField: "dist.calculated",
            spherical : true
        }
      }])
        .sort({dist: 1})
        .skip(from*size)
        .limit(size)
      profile.password = undefined

      if (!profile) {

        // throwing an error if user not found
        return res.status(404).send(
          new apiResponse.responseObject(404, null, {
            message: 'Users not found '
          }).getResObject()
        );
      } else {
        // throwing success response if found
        res.status(200).send(
          new apiResponse.responseObject(200, profile, null).getResObject()
        );
      }
    } catch (err) {
      // throwing any other errors if occured
      console.log(err,'err');
      return res
        .status(400)
        .send(new apiResponse.responseObject(400, null, err).getResObject());
    }
  },

}
