var mongoose = require("mongoose");
var mongoosastic = require("mongoosastic");
var bcrypt = require("bcryptjs");
var crypto = require("crypto");
var jwt = require("jsonwebtoken");

var UserSchema = new mongoose.Schema(
  {
    name:{
      type: String,
      required: true,
    },
    mobile: {
      type: String,
      unique: true,
      required: true
    },
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true,
    },
    address: {
      street: { 
        type: String 
      },
      locality: { 
        type: String 
      },
      city: { 
        type: String 
      },
      state: { 
        type: String 
      },
      pincode: { 
        type: String 
      },
      coordinatesType: {
        type: String,
        enum: ['Point']
      },
      coordinates: {
          type: [Number],
          index: '2dsphere'
        },
    },
    isEmailVerified: {
       type: Boolean,
    },
    verificationToken:{
      type: String
    },
    password: {
      type: String,
      // required: true
    },
    sessionId: {
      type: String
    },
    status: {
      type: String,
      enum: ["ACTIVE", "DELETED"]
    },
  },
  {
    timestamps: true
  }
);

//authenticate input against database
UserSchema.statics.authenticate = async (email, password) => {

      let getUserByEmail = await User.findOne(
        { email: email },
        { _id: 1, email: 1, password: 1, name: 1  }
      );

      if( !getUserByEmail ){
        throw new Error("User_Not_Exist");
      } else if ( getUserByEmail.isEmailVerified === false ){
        throw new Error("Email_Not_Verified");
      }

      const isValidPassword = await bcrypt.compare(password, getUserByEmail.password);
      if (isValidPassword !== true) {
        throw new Error("Wrong_Password")
      }

      const sessionId = await crypto.randomBytes(20).toString("hex");
      let userData = await User.findOneAndUpdate(
        { email: email },
        { $set: { sessionId } },
        { new: true }
      );

      let tokenData = {
        name: userData.name,
        email: userData.email,
        mobile: userData.mobile,
        address: userData.address,
        _id: userData._id
      }

      let jwtToken = jwt.sign( tokenData, process.env.JWT_SECRET, { expiresIn: 86400 }); // expires in 24 hours

      userData.password = undefined;
      userData.token = jwtToken;
      return { data: userData, token: userData.token };
};

//hashing a password before saving it to the database
UserSchema.pre("save", async function(next) {
  var user = this;
  var hashedPassword = await bcrypt.hash(user.password, 10);
  user.password = hashedPassword;
  next();
});

var User = mongoose.model("User", UserSchema);

module.exports = User;
