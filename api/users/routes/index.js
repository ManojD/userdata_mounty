"use strict";
const userController = require("../controllers/user");
const authController = require("../controllers/authController");
const { validate, validateRules } = require("../middlewares/validations");

module.exports = app => {

  // User Authentication Routes

  // Signup
  app.post(
    "/v1/api/auth/signup",
    validateRules("signup"),
    validate,
    authController.signup
  );

  app.get(
    "/v1/api/auth/confirmation/:id",
    authController.tokenVerification
  );
  
  // Signin
  app.post(
    "/v1/api/auth/signin",
    validateRules("signin"),
    validate,
    authController.signin
  );

  // verify token
  app.get(
    "/v1/api/auth/verify/access-token",
    authController.verifyToken
  );

  // Logout
  app.post(
    "/v1/api/auth/signout",
    validateRules("signout"),
    validate,
    authController.signout
  );

  // create user
  app.post(
    "/v1/api/user/create",
    validateRules("create"),
    validate,
    authController.verifyToken,
    userController.create
  );

  // update user
  app.put(
    "/v1/api/user/update/:id",
    authController.verifyToken,
    userController.update
  );

  // delete user
  app.post(
    "/v1/api/user/delete/:id",
    validateRules("delete"),
    validate,
    authController.verifyToken,
    userController.delete
  );

  // get all users
  app.get(
    "/v1/api/user/getAll",
    userController.getAll
  );

  // Get all Users sorted by createdAt timestamp with Pagination
  app.get(
    "/v1/api/user/getByCreatedAt",
    authController.verifyToken,
    userController.getByCreatedAt
  );

  // Get all Users sorted by their distance from coordinates passed in the query param.
  app.get(
    "/v1/api/user/getByCoordinates",
    validateRules("getByCoordinates"),
    validate,
    authController.verifyToken,
    userController.getByCoordinates
  );
  
};
