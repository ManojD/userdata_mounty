module.exports = {
  responseObject: function(code, body, err) {
    this.code = code;
    this.body = body;
    this.error = err;

    this.getResObject = function() {
      if (this.error) {
        return {
          status: this.code,
          payload: this.body,
          error: this.error
        };
      } else {
        return {
          status: this.code,
          payload: this.body
        };
      }
    };
  }
};
