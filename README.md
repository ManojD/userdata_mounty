# User Data Backend
The User Data backend repository which contian all services in single project. 

## Prerequisites
1. Node version required: `^10.0.0`

## Features

- Uses [npm](https://npmjs.com)
- No transpilers, just vanilla javascript with ES2018 latest features like Async/Await
- Express + MongoDB ([Mongoose](http://mongoosejs.com/))
- CORS enabled and uses [helmet](https://github.com/helmetjs/helmet) to set some HTTP headers for security
- Load environment variables from .env files with [dotenv](https://github.com/rolodato/dotenv-safe)
- Request validation with [express-validator](https://github.com/hapijs/joi)
- Logging with winston [winston](https://github.com/winstonjs/winston)
- Logging with [morgan](https://github.com/expressjs/morgan)
- Authentication and Authorization with [passport](http://passportjs.org)
- Rate limiting with [express-rate-limit](https://www.npmjs.com/package/express-rate-limit)

## Dependencies

* node
* express
* bcryptjs
* body-parser
* cors
* dotenv
* express
* jsonwebtoken
* lodash 
* mongoose
* morgan
* winston

## Getting Started
1. Clone this repository.
2. Run `npm install`
3. Create your feature branch and start working on it.
4. Add `.env.dev` file.
5. To start Development environment use `NODE_ENV=dev PORT=7000 node server.js`

Update the .env.dev details locally
<h2>.env.sample</h2>

PORT=

# Base URLs
BASE_URL = ""

JWT_SECRET=

# Mongo Config
MONGO_URI=""
MONGO_DB_NAME=""
MONGO_USERNAME=""
MONGO_PASSWORD=""

## Application Structure

- `server.js` - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application.
- `config/` - This folder contains configuration for passport as well as a central location for configuration/environment variables.
- `api/{moduleName}/routes/` - This folder contains the route definitions for our API.
- `api/{moduleName}/models/` - This folder contains the schema definitions for our Mongoose models.
- `api/{moduleName}/controllers/` - This folder contains the controllers which are linked by API .
- `api/{moduleName}/utils/` - This folder contains the services whcih are common methods across this applcaition.

## Logging
1. We are using `winston` and `morgon` for logging
2. Always use `logger.log` instead of `console.log` 

## Database
we can use any persistant DB for this service. We are using [MongoDB]  with node.js.

## API Guidelines
1. Always implement `/ping` api for every microservice so that we can configure health checks on this url. (`PingRoute.js` is already implemented. Please dont edit or delete this)  