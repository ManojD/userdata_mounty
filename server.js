global.config = require("./config");

const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { Connect } = require("./utils/db");
const logger = require("./utils/logger");
const morganConfig = require("./utils/morgan");
const routes = require("./api");
const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: "50mb", extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
morganConfig(app);

// Ping url
app.route("/v1/api/ping").get((req, res) => {
  res.status(200).send({
    message: "Pong",
    status: "Success"
  });
});

routes(app);


if (require.main === module) {
  Connect(); //dbconnect
  app.listen(config.server.PORT, err => {
    if (err) {
      logger.error({ err });
      process.exit(1);
    }
    logger.info(
      `APP is now running on port ${config.server.PORT} in ${config.server.ENV} mode`
    );
  });
}

module.exports = app;
